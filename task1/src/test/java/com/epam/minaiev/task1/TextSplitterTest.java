package com.epam.minaiev.task1;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

/**
 * Tests for text splitter classes
 */
@RunWith(Enclosed.class)
public class TextSplitterTest {

    @Ignore
    public static abstract class TestsForAnyStorage {

        protected File testFile1 = new File(this.getClass().getClassLoader().getResource("").getPath() + "test1.txt");
        protected TextSplitter textSplitter;

        protected abstract void initSplitterForStorage();

        @Before
        public void before() {
            initSplitterForStorage();
        }

        @Test
        public void shouldReturnCountOfUniqueWord_whenReadingFile() throws Exception {
            assertEquals(textSplitter.splitAngGetUniqueCount(), 11);
        }

        @Test
        public void shouldReturnCountOfWordOccurences_whenStringGivenAsParameter() {
            textSplitter.splitAngGetUniqueCount();
            assertEquals(textSplitter.getWordCount("one"), 1);
            assertEquals(textSplitter.getWordCount("two"), 2);
            assertEquals(textSplitter.getWordCount("three"), 3);
            assertEquals(textSplitter.getWordCount("nosuchword"), 0);
        }

        @Test
        public void shouldReturnConstantResult_whenUniqueCountRequested() {
            assertEquals(11, textSplitter.splitAngGetUniqueCount());
            assertEquals(11, textSplitter.splitAngGetUniqueCount());
        }

        @Test
        public void shouldIgnoreCase_whenWordCountRequested() {
            textSplitter.caseInsensitive = true;
            textSplitter.splitAngGetUniqueCount();

            assertEquals(4, textSplitter.getWordCount("two"));
            assertEquals(4, textSplitter.getWordCount("Two"));
            assertEquals(4, textSplitter.getWordCount("TWo"));
        }

        @Test
        public void shouldNotIgnoreCase_whenWordCountRequested() {
            textSplitter.caseInsensitive = false;
            textSplitter.splitAngGetUniqueCount();

            assertEquals(2, textSplitter.getWordCount("two"));
            assertEquals(1, textSplitter.getWordCount("Two"));
            assertEquals(1, textSplitter.getWordCount("TWo"));
        }

        @Test(expected = NullPointerException.class)
        public void shouldThrowNullPointer_ifNullInParameter() {
            textSplitter.splitAngGetUniqueCount();
            textSplitter.getWordCount(null);
        }
    }

    public static class MapTextSplitterTests extends TestsForAnyStorage {

        @Override
        protected void initSplitterForStorage() {
            textSplitter = new MapTextSplitter(testFile1, false, new HashMap<String, Integer>());
        }
    }

    public static class ListTextSplitterTests extends TestsForAnyStorage {

        @Override
        protected void initSplitterForStorage() {
            textSplitter = new ListTextSplitter(testFile1, false, new ArrayList<ListTextSplitter.Word>());
        }
    }

    public static class SetTextSplitterTests extends TestsForAnyStorage {

        @Override
        protected void initSplitterForStorage() {
            textSplitter = new SetTextSplitter(testFile1, false, new HashSet<ListTextSplitter.Word>());
        }
    }
}
