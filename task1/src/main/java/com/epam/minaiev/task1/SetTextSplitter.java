package com.epam.minaiev.task1;

import java.io.File;
import java.util.Set;

/**
 *  Splits this text file into words and stores them in the provided Set with the number of repetitions
 */
public class SetTextSplitter extends TextSplitter {
    private Set<ListTextSplitter.Word> storage;

    public SetTextSplitter(File file, boolean caseInsensitive, Set<ListTextSplitter.Word> storage) {
        if (storage == null || file == null) {
            throw new NullPointerException();
        }
        this.storage = storage;
        this.file = file;
        this.caseInsensitive = caseInsensitive;
    }

    @Override
    protected int getWordCountFromStorage(String word) {
        ListTextSplitter.Word newWord = new ListTextSplitter.Word(word);
        if (storage.contains(newWord)) {
            for (ListTextSplitter.Word w : storage) {
                if (w.equals(newWord)) {
                    return w.getCount();
                }
            }
        }
        return 0;
    }

    @Override
    protected void addToStorage(String word) {
        ListTextSplitter.Word newWord = new ListTextSplitter.Word(word);
        if (!storage.contains(newWord)) {
            storage.add(newWord);
        } else {
            for (ListTextSplitter.Word w : storage) {
                if (w.equals(newWord)) {
                    w.incrementCount();
                    break;
                }
            }
        }
    }

    @Override
    protected int getUniqueWordsCount() {
        return storage.size();
    }

    @Override
    protected void clearStorage() {
        storage.clear();
    }
}
