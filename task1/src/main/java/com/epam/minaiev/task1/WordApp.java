package com.epam.minaiev.task1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Main class of application
 *
 * @author Leoid Minaiev
 */
public class WordApp {
    private File textFile;
    private TextSplitter textSplitter;
    private Boolean caseInsensitive;
    private Object storage;
    BufferedReader input;

    public static void main(String[] args) throws IOException {
        new WordApp().start();
    }

    public WordApp() {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public void start() throws IOException {
        init();
        String word;
        while ((word = input.readLine()) != null) {
            switch (word) {
                case "-exit":
                    System.out.println("Good bye!");
                    return;
                case "-restart":
                    restart();
                    break;
                default:
                    System.out.printf("Word \"%s\" is used %d times.%n", word, textSplitter.getWordCount(word));
                    break;
            }
        }
    }

    private void init() throws IOException {
        selectFile();
        selectCase();
        selectStorage();
        calculateWords();
    }

    private void restart() throws IOException {
        textFile = null;
        storage = null;
        caseInsensitive = null;
        init();
    }

    private void calculateWords() {
        checkState(storage, textSplitter, caseInsensitive);
        System.out.println("Please wait...");
        long startTime = System.currentTimeMillis();
        int result = textSplitter.splitAngGetUniqueCount();
        long endTime = System.currentTimeMillis();

        System.out.printf("Text file \"%s\" has %d unique words.%n", textFile.getAbsolutePath(), result);
        System.out.printf("Processing time: %d ms.%n", (endTime - startTime));
        System.out.println("Enter word to display the number of its repetitions or \"-exit\" to exit the application or \"-restart\" to restart application.");
    }

    private void selectStorage() throws IOException {

        System.out.println("Choose type of words storage");
        System.out.println("\t1) HashMap");
        System.out.println("\t2) TreeMap");
        System.out.println("\t3) ArrayList");
        System.out.println("\t4) LinkedList");
        System.out.println("\t5) HashSet");
        System.out.println("\t6) TreeSet");

        while (storage == null) {
            String inputString = input.readLine();
            switch (inputString) {
                case "1":
                    storage = new HashMap<String, Integer>();
                    break;
                case "2":
                    storage = new TreeMap<String, Integer>();
                    break;
                case "3":
                    storage = new ArrayList<ListTextSplitter.Word>();
                    break;
                case "4":
                    storage = new LinkedList<ListTextSplitter.Word>();
                    break;
                case "5":
                    storage = new HashSet<ListTextSplitter.Word>();
                    break;
                case "6":
                    storage = new TreeSet<ListTextSplitter.Word>();
                    break;
            }
        }
        if (storage instanceof Map) {
            textSplitter = new MapTextSplitter(textFile, caseInsensitive, (Map<String, Integer>) storage);
        } else if (storage instanceof List) {
            textSplitter = new ListTextSplitter(textFile, caseInsensitive, (List<ListTextSplitter.Word>) storage);
        } else if (storage instanceof Set) {
            textSplitter = new SetTextSplitter(textFile, caseInsensitive, (Set<ListTextSplitter.Word>) storage);
        }
    }

    private void selectFile() throws IOException {
        System.out.println("Enter filename to read words from");
        while (textFile == null || !textFile.isFile()) {
            String inputString = input.readLine();
            textFile = new File(inputString);
            if (!textFile.isFile()) {
                System.out.println("File not found! Please select an existing file");
            }
        }
    }

    private void selectCase() throws IOException {
        System.out.println("Use case sensitive search? y/n");
        while (caseInsensitive == null) {
            String inputString = input.readLine();
            if (inputString.toLowerCase().equals("y")) {
                caseInsensitive = false;
            } else if (inputString.toLowerCase().equals("n")) {
                caseInsensitive = true;
            }
        }
    }

    private void checkState(Object... objects) {
        for (Object o : objects) {
            if (o == null) {
                throw new IllegalStateException();
            }
        }
    }
}
