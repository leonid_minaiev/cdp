package com.epam.minaiev.task1;

import java.io.File;
import java.util.List;

/**
 * Splits this text file into words and stores them in the provided List with the number of repetitions
 */
public class ListTextSplitter extends TextSplitter {
    private List<Word> storage;

    public ListTextSplitter(File file, boolean caseInsensitive, List<Word> storage) {
        if (storage == null || file == null) {
            throw new NullPointerException();
        }
        this.storage = storage;
        this.file = file;
        this.caseInsensitive = caseInsensitive;
    }

    @Override
    protected int getWordCountFromStorage(String word) {
        int index = storage.indexOf(new Word(word));
        if (index > -1) {
            Word w = storage.get(index);
            return w.getCount();
        }
        return 0;
    }

    @Override
    protected void addToStorage(String word) {
        Word newWord = new Word(word);
        int index = storage.indexOf(newWord);
        if (index > -1) {
            Word word1 = storage.get(index);
            word1.incrementCount();
        } else {
            storage.add(newWord);
        }
    }

    @Override
    protected int getUniqueWordsCount() {
        return storage.size();
    }

    @Override
    protected void clearStorage() {
        storage.clear();
    }

    /**
     * Class Word represents one word string with number of its occurrences in the text
     *
     * @author Leonid Minaiev
     */
    public static class Word implements Comparable<Word> {
        private int count;
        private final String value;

        /**
         * Constructs new Word object with default occurrences count = 1;
         *
         * @param value word string
         */
        public Word(String value) {
            this.value = value;
            count = 1;
        }

        /**
         * Constructs new Word object;
         *
         * @param value word string
         * @param count word occurrences
         */
        public Word(String value, int count) {
            if (value == null){
                throw new NullPointerException();
            }
            this.value = value;
            this.count = count;
        }

        public void incrementCount() {
            count++;
        }

        public int getCount() {
            return count;
        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Word && value.equals(((Word) obj).value);
        }

        @Override
        public String toString() {
            return value;
        }

        @Override
        public int compareTo(Word o) {
            return (value.compareTo(o.value));
        }
    }
}