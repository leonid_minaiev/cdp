package com.epam.minaiev.task1;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstract Text Splitter. Splits this text file to the words
 */
abstract class TextSplitter {
    File file;
    private final Pattern pattern = Pattern.compile("[\\wа-яА-Я']+");
    protected boolean caseInsensitive;

    /**
     * Reads text from file and splits it to the words.
     *
     * @return count of unique words in text
     */
    public int splitAngGetUniqueCount() {
        clearStorage();
        BufferedReader bufferedReader = null;
        String line;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            while ((line = bufferedReader.readLine()) != null) {
                Matcher m = pattern.matcher(line);
                while (m.find()) {
                    String word = m.group();
                    if (caseInsensitive) {
                        addToStorage(word.toLowerCase());
                    } else {
                        addToStorage(word);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return getUniqueWordsCount();
    }

    public int getWordCount(String word) {
        if (word == null) {
            throw new NullPointerException();
        }
        if (caseInsensitive) {
            return getWordCountFromStorage(word.toLowerCase());
        } else {
            return getWordCountFromStorage(word);
        }

    }

    /**
     * Returns count of word occurrences
     *
     * @param word word string
     * @return count of occurrences
     */
    protected abstract int getWordCountFromStorage(String word);

    /**
     * Adds word to storage or increases count of its uses if already exists
     *
     * @param word word string
     */
    protected abstract void addToStorage(String word);

    /**
     * Returns count of unique words in file
     *
     * @return count of words
     */
    protected abstract int getUniqueWordsCount();

    /**
     * Clears word storage
     */
    protected abstract void clearStorage();
}
