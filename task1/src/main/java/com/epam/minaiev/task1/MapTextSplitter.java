package com.epam.minaiev.task1;

import java.io.File;
import java.util.Map;

/**
 * Splits this text file into words and stores them in the provided Map with the number of repetitions
 */
public class MapTextSplitter extends TextSplitter {
    private Map<String, Integer> storage;

    public MapTextSplitter(File file, boolean caseInsensitive, Map<String, Integer> storage) {
        if (storage == null || file == null) {
            throw new NullPointerException();
        }
        this.file = file;
        this.storage = storage;
        this.caseInsensitive = caseInsensitive;
    }

    @Override
    protected int getWordCountFromStorage(String word) {
        Integer result = storage.get(word);
        return result == null ? 0 : result;
    }

    @Override
    protected void addToStorage(String word) {
        if (storage.containsKey(word)) {
            int count = storage.get(word);
            storage.put(word, ++count);
        } else {
            storage.put(word, 1);
        }
    }

    @Override
    protected int getUniqueWordsCount() {
        return storage.size();
    }

    @Override
    protected void clearStorage() {
        storage.clear();
    }
}
