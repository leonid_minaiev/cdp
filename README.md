#CDP tasks solved by Leonid Minaiev

##Task1:

1. Consider a program that manages an appointment calendar. Should it place the appointments into a list, stack, queue, or priority queue? Explain your answer.
2. Suppose you need to organize a collection of telephone numbers for a company division. There are currently about 6,000 employees, and you know that the phone switch can handle at most 10,000 phone numbers. You expect several hundred lookups against the collection every day. What kind of collection would you use to store information? Explain your answer.
3. Write an application that gets all words from a large file (such as the novel “War and Peace”, which is available on the Internet) and store it somehow in the memory.
    - User can enter any word and the application should show how many times this world is used in this file.
    - User can get information about how many unique words the text file has.
    - Time the results with different type of collections . Which data structure is more efficient for this task?